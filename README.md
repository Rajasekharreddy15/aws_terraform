# Aws_terraform

This is a repo for setting up aws infrastructure with terraform.

## What is Terraform

Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

## Installation

You can download binary file from [here](https://www.terraform.io/downloads.html) and store it into you environment variable path.

